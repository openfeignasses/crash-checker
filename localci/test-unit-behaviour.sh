set -e # exit on fail

if [ ! -d "../database-migrator" ]; then
    echo "Directory ../commons-schema doesn't exist. Exiting ..."
    exit 1
fi
echo "Recompiling code ..."
docker build -t crash-checker --target testable . || exit 1
cd ../database-migrator
docker build -t database-migrator . || exit 1
cd ../crash-checker
echo "Running tests ..."
running_db_ip=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' mysql)
docker run \
    --link mysql:mysql \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e DATABASE_USER=admin \
    -e DATABASE_PASSWORD=frNH95eSiLYY \
    -e DATABASE_DB=cryptobot \
    database-migrator
docker run \
    --link mysql:mysql \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e DATABASE_USER=admin \
    -e DATABASE_PASSWORD=frNH95eSiLYY \
    -e DATABASE_DB=cryptobot \
    database-migrator npm run clear-db
docker run \
    --link mysql:mysql \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e DATABASE_USER=admin \
    -e DATABASE_PASSWORD=frNH95eSiLYY \
    -e DATABASE_DB=cryptobot \
    database-migrator npm run fixtures:crash-checker:test
docker run \
    --rm \
    --add-host=mysql:$running_db_ip \
    -e MOCK_KRAKEN_API=true \
    -e MYSQL_DATABASE=cryptobot \
    -e MYSQL_USER=admin \
    -e MYSQL_PASSWORD=frNH95eSiLYY \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e MYSQL_USE_SSL=false \
    crash-checker bash -c "mvn -o test -Dcucumber.options='--tags \"not @WIP\" --plugin pretty'"