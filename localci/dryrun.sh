set -e # exit on failure
if [ ! -d "../database-migrator" ]; then
    echo "Directory ../commons-schema doesn't exist. Exiting ..."
    exit 1
fi
echo "Recompiling code ..."
docker build -t crash-checker .
cd ../database-migrator
docker build -t database-migrator .
cd ../crash-checker
echo "Executing ..."
running_db_ip=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' mysql)
docker run \
    --link mysql:mysql \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e DATABASE_USER=admin \
    -e DATABASE_PASSWORD=frNH95eSiLYY \
    -e DATABASE_DB=cryptobot \
    database-migrator
docker run \
    --link mysql:mysql \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e DATABASE_USER=admin \
    -e DATABASE_PASSWORD=frNH95eSiLYY \
    -e DATABASE_DB=cryptobot \
    database-migrator npm run clear-db
docker run \
    --link mysql:mysql \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e DATABASE_USER=admin \
    -e DATABASE_PASSWORD=frNH95eSiLYY \
    -e DATABASE_DB=cryptobot \
    database-migrator npm run fixtures:crash-checker:dryrun
docker run \
    --rm \
    --add-host=mysql:$running_db_ip \
    -e MYSQL_DATABASE=cryptobot \
    -e MYSQL_USER=admin \
    -e MYSQL_PASSWORD=frNH95eSiLYY \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e MYSQL_USE_SSL=false \
    -e CRASH_THRESHOLD=10 \
    crash-checker