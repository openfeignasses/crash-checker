@CrashChecker
@CrashChecker_ServerAndNetworkErrors
@PlatformIndependent
@Stub
Feature: Crash checker doesn't change statuses to non tradable

   As Bernard the Crash Checker
   In order to stop loss one currency when a crash is occuring,
   I want the Crash Checker to monitor quotations and detect it as
   soon as possible. The module must properly communicate with the
   configured platform. The module should't change the status of
   a currency if a communication problem occurs with the server.

   Background:
      Given the frequency is "4" hours
      And the "BTC_EUR" currency is marked as "TRADABLE"
      And the current date is "10/04/2020 17:20"

   @StockQuotationsProvider_Get_1
   Scenario: Server error
      Given the error field in the server response contains one element for "BTC_EUR"
      And it is not a warning
      When I try to run the crash checker
      Then the "BTC_EUR" currency is flagged as "TRADABLE"

   @StockQuotationsProvider_Get_2
   Scenario: Server warning
      Given the error field in the server response contains one element for "BTC_EUR"
      And it is a warning
      When I try to run the crash checker
      Then the "BTC_EUR" currency is flagged as "TRADABLE"

   @StockQuotationsProvider_Get_3
   Scenario: Communication error
      Given the server is not returning a response while requesting "BTC_EUR"
      And the request timeout is 10 seconds
      When I try to run the crash checker
      Then the "BTC_EUR" currency is flagged as "TRADABLE"