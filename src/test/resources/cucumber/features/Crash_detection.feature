@CrashChecker
@CrashChecker_happy
@PlatformIndependent
@Stub
Feature: We are able to detect a crash

   As __ the Crash Checker
   In order to stop loss one currency when a crash is occuring,
   I want the Crash Checker to monitor quotations and detect it as 
   soon as possible. A loss greater than or equal to the crash threshold 
   since the last execution is considered as a crash.

   Background: Global config
      Given the crash threshold is "-10.0"%

   @CrashChecker_happy_1
   Scenario: Crash way above the crash threshold
      Given the frequency is "1" hour
      And the period is "1" day
      And the first date is "01/01/2020 12:00"
      And the last date is "02/01/2020 12:00"
      And the current date is "02/01/2020 12:25"
      And the "BTC_EUR" currency is marked as "TRADABLE"
      And that the signal for "BTC_EUR" on "01/01/2020 17:50" is "LONG"
      And we bought "BTC_EUR" on "01/01/2020 18:00" at value "10"
      And the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------------+
      |         xxx            |
      |        x   x           |
      |       x     x          |
      |      x                 |
      |     x        x         |
      |    x                   |
      |xxxx           x        |
      |                        |
      |                x       |
      |                 x      |
      |                  x     |
      |                   x    |
      |                    x   |
      |                     xxx|
      +------------------------+
             ^
             |
            last buy
      """
      When I run the crash checker
      Then I observe a variation of "-100"% for "BTC_EUR"
      And the "BTC_EUR" currency is flagged as "CRASHING"

   @CrashChecker_happy_2
   Scenario: Crash percent value same as the crash threshold
      Given the frequency is "1" hour
      And the period is "1" day
      And the first date is "01/01/2020 12:00"
      And the last date is "02/01/2020 12:00"
      And the current date is "02/01/2020 12:25"
      And the "BTC_EUR" currency is marked as "TRADABLE"
      And that the signal for "BTC_EUR" on "01/01/2020 17:50" is "LONG"
      And we bought "BTC_EUR" on "01/01/2020 18:00" at value "10"
      And the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------------+
      |      xxxxxxxxxxxxxxxxx |
      |     x                 x|
      |    x                   |
      |  xx                    |
      |xx                      |
      |                        |
      |                        |
      |                        |
      |                        |
      |                        |
      |                        |
      +------------------------+
             ^
             |
            last buy
      """
      When I run the crash checker
      Then I observe a variation of "-10.0"% for "BTC_EUR"
      And the "BTC_EUR" currency is flagged as "CRASHING"

   @CrashChecker_happy_3
   Scenario: Not crashing
      Given the frequency is "1" hour
      And the period is "1" day
      And the first date is "01/01/2020 12:00"
      And the last date is "02/01/2020 12:00"
      And the current date is "02/01/2020 12:25"
      And the "BTC_EUR" currency is marked as "TRADABLE"
      And that the signal for "BTC_EUR" on "01/01/2020 17:50" is "LONG"
      And we bought "BTC_EUR" on "01/01/2020 18:00" at value "10"
      And the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------------+
      |      xxxxxxxxxxxxxxxxxx|
      |     x                  |
      |    x                   |
      |  xx                    |
      |xx                      |
      |                        |
      |                        |
      |                        |
      |                        |
      |                        |
      |                        |
      +------------------------+
             ^
             |
            last buy
      """
      When I run the crash checker
      Then I observe a variation of "0"% for "BTC_EUR"
      And the "BTC_EUR" currency is flagged as "TRADABLE"

   @CrashChecker_happy_4
   Scenario: The currency was already marked as crashing, and still crashing
      Given the frequency is "1" hour
      And the period is "1" day
      And the first date is "01/01/2020 12:00"
      And the last date is "02/01/2020 12:00"
      And the current date is "02/01/2020 12:25"
      And the "BTC_EUR" currency is marked as "CRASHING"
      And that the signal for "BTC_EUR" on "01/01/2020 17:50" is "LONG"
      And we bought "BTC_EUR" on "01/01/2020 18:00" at value "8"
      And the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------------+
      |x                       |
      | x                      |
      |  xx                    |
      |    x                   |
      |     x                  |
      |      xx                |
      |        xxxxxxxx        |
      |                x       |
      |                        |
      |                 x      |
      |                  x     |
      |                   x    |
      |                    x   |
      |                     xxx|
      +------------------------+
             ^
             |
            last buy
      """
      When I run the crash checker
      Then the "BTC_EUR" currency is flagged as "CRASHING"

   @CrashChecker_happy_5
   Scenario: The currency was already marked as crashing, not crashing anymore
      Given the frequency is "1" hour
      And the period is "1" day
      And the first date is "01/01/2020 12:00"
      And the last date is "02/01/2020 12:00"
      And the current date is "02/01/2020 12:25"
      And the "BTC_EUR" currency is marked as "CRASHING"
      And that the signal for "BTC_EUR" on "01/01/2020 17:50" is "LONG"
      And we bought "BTC_EUR" on "01/01/2020 18:00" at value "8"
      And the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------------+
      |x                       |
      | x                      |
      |  xx                    |
      |    x                   |
      |     x              xxxx|
      |      xx        xxxx    |
      |        xxxxxxxx        |
      |                        |
      |                        |
      |                        |
      |                        |
      |                        |
      |                        |
      |                        |
      +------------------------+
             ^
             |
            last buy
      """
      When I run the crash checker
      Then the "BTC_EUR" currency is flagged as "CRASHING"

   @CrashChecker_happy_6
   Scenario: The currency is marked as non-tradable
      Given the frequency is "1" hour
      And the period is "1" day
      And the first date is "01/01/2020 12:00"
      And the last date is "02/01/2020 12:00"
      And the current date is "02/01/2020 12:25"
      And the "BTC_EUR" currency is marked as "NON_TRADABLE"
      When I run the crash checker
      Then the "BTC_EUR" currency is flagged as "NON_TRADABLE"

   @CrashChecker_happy_7
   Scenario: Currency is SHORT
      Given the frequency is "1" hour
      And the period is "1" day
      And the first date is "01/01/2020 12:00"
      And the last date is "02/01/2020 12:00"
      And the current date is "02/01/2020 12:25"
      And the "BTC_EUR" currency is marked as "TRADABLE"
      And that the signal for "BTC_EUR" on "01/01/2020 17:50" is "SHORT"
      And we bought "BTC_EUR" on "01/01/2020 18:00" at value "10"
      And the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------------+
      |         xxx            |
      |        x   x           |
      |       x     x          |
      |      x                 |
      |     x        x         |
      |    x                   |
      |xxxx           x        |
      |                        |
      |                x       |
      |                 x      |
      |                  x     |
      |                   x    |
      |                    x   |
      |                     xxx|
      +------------------------+
             ^
             |
            last buy
      """
      When I run the crash checker
      And the "BTC_EUR" currency is flagged as "TRADABLE"

   @CrashChecker_happy_8
   Scenario: Currency is NEUTRAL
      Given the frequency is "1" hour
      And the period is "1" day
      And the first date is "01/01/2020 12:00"
      And the last date is "02/01/2020 12:00"
      And the current date is "02/01/2020 12:25"
      And the "BTC_EUR" currency is marked as "TRADABLE"
      And that the signal for "BTC_EUR" on "01/01/2020 17:50" is "NEUTRAL"
      And we bought "BTC_EUR" on "01/01/2020 18:00" at value "10"
      And the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------------+
      |         xxx            |
      |        x   x           |
      |       x     x          |
      |      x                 |
      |     x        x         |
      |    x                   |
      |xxxx           x        |
      |                        |
      |                x       |
      |                 x      |
      |                  x     |
      |                   x    |
      |                    x   |
      |                     xxx|
      +------------------------+
             ^
             |
            last buy
      """
      When I run the crash checker
      And the "BTC_EUR" currency is flagged as "TRADABLE"