@CrashChecker
@CrashChecker_multiple_happy
@PlatformIndependent
@Stub
Feature: We are able to detect crashes on multiple currencies

   As __ the Crash Checker
   In order to stop loss one or multiple currencies when a crash is occuring,
   I want the Crash Checker to monitor quotations and detect it as 
   soon as possible. A loss greater than or equal to the crash threshold 
   since the last execution is considered as a crash.

   Background: Global config
      Given the crash threshold is "10"%

   @CrashChecker_multiple_happy_1
   Scenario: 2 crashes way above the crash threshold
      Given the frequency is "1" hour
      And the period is "1" day
      And the first date is "01/01/2020 12:00"
      And the last date is "02/01/2020 12:00"
      And the current date is "02/01/2020 12:25"
      And the "BTC_EUR" currency is marked as "TRADABLE"
      And that the signal for "BTC_EUR" on "01/01/2020 17:50" is "LONG"
      And we bought "BTC_EUR" on "01/01/2020 18:00" at value "10"
      And the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------------+
      |         xxx            |
      |        x   x           |
      |       x     x          |
      |      x                 |
      |     x        x         |
      |    x                   |
      |xxxx           x        |
      |                        |
      |                x       |
      |                 x      |
      |                  x     |
      |                   x    |
      |                    x   |
      |                     xxx|
      +------------------------+
             ^
             |
            last buy
      """
      And the "ETH_EUR" currency is marked as "TRADABLE"
      And that the signal for "ETH_EUR" on "01/01/2020 17:50" is "LONG"
      And we bought "ETH_EUR" on "01/01/2020 18:00" at value "10"
      And the "ETH_EUR" stock quotation available on the platform is
      """
      +------------------------+
      |         xxx            |
      |        x   x           |
      |       x     x          |
      |      x                 |
      |     x        x         |
      |    x                   |
      |xxxx           x        |
      |                        |
      |                x       |
      |                 x      |
      |                  x     |
      |                   x    |
      |                    x   |
      |                     xxx|
      +------------------------+
             ^
             |
            last buy
      """
      When I run the crash checker
      Then I observe a variation of "-100"% for "BTC_EUR"
      And I observe a variation of "-100"% for "ETH_EUR"
      And the "BTC_EUR" currency is flagged as "CRASHING"
      And the "ETH_EUR" currency is flagged as "CRASHING"

   @CrashChecker_multiple_happy_2
   Scenario: One currency is crashing, one other is not
      Given the frequency is "1" hour
      And the period is "1" day
      And the first date is "01/01/2020 12:00"
      And the last date is "02/01/2020 12:00"
      And the current date is "02/01/2020 12:25"
      And the "BTC_EUR" currency is marked as "TRADABLE"
      And that the signal for "BTC_EUR" on "01/01/2020 17:50" is "LONG"
      And we bought "BTC_EUR" on "01/01/2020 18:00" at value "10"
      And the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------------+
      |         xxx            |
      |        x   x           |
      |       x     x          |
      |      x                 |
      |     x        x         |
      |    x                   |
      |xxxx           x        |
      |                        |
      |                x       |
      |                 x      |
      |                  x     |
      |                   x    |
      |                    x   |
      |                     xxx|
      +------------------------+
             ^
             |
            last buy
      """
      And the "ETH_EUR" currency is marked as "TRADABLE"
      And that the signal for "ETH_EUR" on "01/01/2020 17:50" is "LONG"
      And we bought "ETH_EUR" on "01/01/2020 18:00" at value "4"
      And the "ETH_EUR" stock quotation available on the platform is
      """
      +------------------------+
      |                        |
      |                        |
      |                        |
      |                        |
      |                        |
      |              xxxxxxxxxx|
      |            xx          |
      |          xx            |
      |        xx              |
      |     xxx                |
      |    x                   |
      |  xx                    |
      | x                      |
      |x                       |
      +------------------------+
             ^
             |
            last buy
      """
      When I run the crash checker
      Then I observe a variation of "-100"% for "BTC_EUR"
      And I observe a variation of "100"% for "ETH_EUR"
      And the "BTC_EUR" currency is flagged as "CRASHING"
      And the "ETH_EUR" currency is flagged as "TRADABLE"