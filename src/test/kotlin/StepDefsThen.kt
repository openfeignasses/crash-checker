import cucumber.api.PendingException
import cucumber.api.java8.En
import io.cucumber.datatable.DataTable
import kraken.KrakenOHLCsPersistable
import model.InvestmentUniverseStatusRetriever
import utils.GherkinQuotationConverter
import java.io.File
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter


class StepDefsThen(var world: World): En {

    init {

        Then("the \"(\\w+_\\w+)\" currency is flagged as \"([\\w_]+)\"") { pair: String, expectedStatus: String ->
            assert(InvestmentUniverseStatusRetriever().findByPair(pair).status == expectedStatus)
        }

        Then("the last \"(\\w+_\\w+)\" price accessible by other modules is \"(\\d+)\"") { pair: String, price: Int ->
            assert(world.result == BigDecimal(price))
        }

        Then("^we obtain these values for \"(\\w+_\\w+)\"$") { pair: String, table: DataTable ->
            var expectedValuesList = GherkinQuotationConverter(table.asList()[0]).getValues()
            val krakenOHLCsPersistables = KrakenOHLCsPersistable.findAll(pair)
            var index = 0
            expectedValuesList.forEach {
                assert(it.toBigDecimal().setScale(8) == krakenOHLCsPersistables[index++].close)
            }
        }

        Then("^we obtain this values table for \"(\\w+_\\w+)\"$") { pair: String, table: DataTable ->
            var table = table.asLists()
            table = table.subList(1, table.size)
            println("table:"+table)

            var expectedValuesList = table
            val krakenOHLCsPersistables = KrakenOHLCsPersistable.findAll(pair)
            var index = 0
            expectedValuesList.forEach {
                assert(it.get(0).toLong() == krakenOHLCsPersistables[index].time)
                assert(it.get(1).toBigDecimal().setScale(8) == krakenOHLCsPersistables[index].close)
                index++
            }
        }

        Then("^there (?:is|are) \"(\\d+)\" -1 value(?:s|) in the result$") { expectedNumberOfMinusOneValues: Int ->
            val krakenOHLCsPersistables = KrakenOHLCsPersistable.findAll(world.pair)
            var count: Int = 0
            krakenOHLCsPersistables.forEach {
                if (it.close == (-1).toBigDecimal().setScale(8)) count++
            }
            assert(expectedNumberOfMinusOneValues == count)
        }

        Then("^the number of returned values is (\\d+)$") { expectedNumberOfValues: Int ->
            val krakenOHLCsPersistables = KrakenOHLCsPersistable.findAll(world.pair)
            println("______________ krakenOHLCsPersistables.size:"+krakenOHLCsPersistables.size)
            assert(krakenOHLCsPersistables.size == expectedNumberOfValues)
        }

        Then("^the number of returned values is (\\d+) for \"(\\w+_\\w+)\"$") {
            expectedNumberOfValues: Int,
            pair: String ->
            val resultsForPair = KrakenOHLCsPersistable.findAll(pair)
            assert(resultsForPair.size == expectedNumberOfValues)
        }

        Then("^the first date of the returned values is \"([\\d/ :]+)\"$") { expectedFirstDate: String ->
            val krakenOHLCsPersistables = KrakenOHLCsPersistable.findAll(world.pair)
            val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")
            val dateAsEpochSeconds = LocalDateTime.parse(expectedFirstDate, formatter).toEpochSecond(ZoneOffset.UTC)

            assert(krakenOHLCsPersistables.first().time == dateAsEpochSeconds)
        }

        Then("^the last date of the returned values is \"([\\d/ :]+)\"$") { expectedLastDate: String ->
            val krakenOHLCsPersistables = KrakenOHLCsPersistable.findAll(world.pair)
            val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")
            val dateAsEpochSeconds = LocalDateTime.parse(expectedLastDate, formatter).toEpochSecond(ZoneOffset.UTC)
            assert(krakenOHLCsPersistables.last().time == dateAsEpochSeconds)
        }

        Then("^a warning file is created$") {
            assert(File("warnings").listFiles().isNotEmpty())
        }

        Then("^an error is thrown$") {
            assert(world.errorOccured)
        }

        Then("I observe a variation of \"(-?\\d+.?\\d*)\"% for \"(\\w+_\\w+)\"") {
                variation: BigDecimal,
                pair: String ->
            println("world.variations: "+world.variations)
            println("BigDecimal(variation): "+variation)
            assert(world.variations[pair] == variation)
        }

        Then("I observe a variation above the crash threshold for \"(\\w+_\\w+)\"") {
                pair: String ->
            println("world.variations: "+world.variations)
            assert(world.variations[pair]?.abs()!! > world.crashThreshold.abs())
        }

    }
}