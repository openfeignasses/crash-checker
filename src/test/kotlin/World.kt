import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder
import org.joda.time.DateTime
import utils.KrakenApiResultsErrorGenerator
import java.math.BigDecimal

class World {
    var variations: Map<String, BigDecimal> = mutableMapOf()
    var crashThreshold: BigDecimal = BigDecimal(-1)
    lateinit var stubResponses: Map<String, Map<Int, ResponseDefinitionBuilder>>
    var stubResponseFor1HourFreqency: ResponseDefinitionBuilder? = null
    var frequencyForFollowingTable: Int = -1
    lateinit var currentDate: String
    lateinit var lastDate: String
    lateinit var firstDate: String
    var period: Int = -1
    var frequency: Int = -1
    var errorOccured: Boolean = false
    var apiErrorGenerator = KrakenApiResultsErrorGenerator()
    var stubResponseFor4HoursFreqency: ResponseDefinitionBuilder? = null
    var result: BigDecimal = BigDecimal(-1)
    var pair: String = ""
    lateinit var wireMockServer: WireMockServer
}