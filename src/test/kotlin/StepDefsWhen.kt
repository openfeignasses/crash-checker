import cucumber.api.java8.En

class StepDefsWhen(var world: World): En {

    init {

        When("I (try to |)run the crash checker") {
            try {
                val crashChecker = CrashChecker(world.crashThreshold)
                world.variations = crashChecker.runGetVariations()
                crashChecker.detectAndAnnounceCrash(world.variations)
            } catch (e: Exception) {
                e.printStackTrace()
                world.errorOccured = true
            }
        }
    }
}