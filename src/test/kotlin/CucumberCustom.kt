import cucumber.api.junit.Cucumber
import utils.PersistenceConfigOverride

class CucumberCustom(clazz: Class<*>?) : Cucumber(clazz) {
    init {
        PersistenceConfigOverride.setDatabaseConnetion(
                System.getenv("DATABASE_HOST"),
                System.getenv("DATABASE_PORT"),
                System.getenv("MYSQL_DATABASE"),
                System.getenv("MYSQL_USER"),
                System.getenv("MYSQL_PASSWORD"),
                System.getenv("MYSQL_USE_SSL") ?: "true"
        )
    }
}