import com.github.tomakehurst.wiremock.client.WireMock.*
import cucumber.api.PendingException
import cucumber.api.java8.En
import io.cucumber.datatable.DataTable
import kraken.KrakenConnector
import model.InvestmentUniverseStatusUpdater
import model.StrategyResult
import model.TransactionsUpdater
import org.knowm.xchange.currency.CurrencyPair
import repositories.StrategiesResultsRepository
import utils.GherkinQuotationConverter
import utils.KrakenApiResultsErrorGenerator
import utils.KrakenApiResultsGenerator
import java.math.BigDecimal
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter


class StepDefsGiven(var world: World, val krakenConnector: KrakenConnector): En {

    init {

        Given("the request timeout is (\\d+) seconds") { timeout: Int ->
            krakenConnector.httpConnTimeout = timeout * 1000
            krakenConnector.httpReadTimeout = timeout * 1000
        }

        Given("^the \"(\\w+_\\w+)\" stock quotation table available on the platform is$") {
            pair: String,
            table: DataTable ->

            var table = table.asLists()
            table = table.subList(1, table.size)
            world.pair = pair

            val ticker = "X" + CurrencyPair(pair.replace("_", "/")).base.iso4217Currency + "Z" + CurrencyPair(pair.replace("_", "/")).counter.iso4217Currency

            val generator = KrakenApiResultsGenerator(
                    ticker,
                    Instant.ofEpochSecond(table.get(0).get(0).toLong()).atZone(ZoneOffset.UTC).toLocalDateTime(),
                    4)
            generator.addQuotationsFromTable(table)
            println(generator.jsonObject)

            world.wireMockServer.stubFor(get(urlEqualTo("/0/public/OHLC?pair=$ticker&interval=${world.frequencyForFollowingTable*60}&since=1"))
                    .willReturn(
                            aResponse()
                                    ?.withHeader("Content-Type", "application/json")
                                    ?.withBody(generator.jsonObject.toString())
                    )
            )
        }

        Given("^the \"(\\w+_\\w+)\" stock quotation available on the platform is$") { pair: String,
                                                                                      table: DataTable ->

            world.pair = pair
            val ticker = "X" + CurrencyPair(pair.replace("_", "/")).base.iso4217Currency + "Z" + CurrencyPair(pair.replace("_", "/")).counter.iso4217Currency

            println("world.pair:"+world.pair)
            println("ticker:"+ticker)

            val valuesList = GherkinQuotationConverter(table.asList()[0]).getValues()
            println("||||||| valuesList:"+valuesList)
            var formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")
            val firstDateObject = LocalDateTime.parse(world.firstDate, formatter)
            val generator = KrakenApiResultsGenerator(ticker, firstDateObject, world.frequency)
            generator.addQuotationsFromValuesList(valuesList)

            println(">>> generator.jsonObject.toString():" + generator.jsonObject.toString())

            world.wireMockServer.stubFor(get(urlMatching("/0/public/OHLC\\?pair=$ticker&interval=${world.frequency*60}&since=([0-9]+)"))
                .willReturn(
                    aResponse()
                    ?.withHeader("Content-Type", "application/json")
                    ?.withBody(generator.jsonObject.toString())
                )
            )
        }

        Given("^the \"(\\w+_\\w+)\" stock quotation with duplicates available on the platform is$") {
            pair: String,
            table: DataTable ->

            world.pair = pair
            val ticker = "X" + CurrencyPair(pair.replace("_", "/")).base.iso4217Currency + "Z" + CurrencyPair(pair.replace("_", "/")).counter.iso4217Currency

            val valuesList = GherkinQuotationConverter(table.asList()[0]).getValuesWithDuplicates()
            println("valuesList:"+valuesList)
            var formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")
            val firstDateObject = LocalDateTime.parse(world.firstDate, formatter)
            val generator = KrakenApiResultsGenerator(world.pair, firstDateObject, world.frequency)
            generator.addQuotationsFromValuesListWithDuplicates(valuesList)

            println(">>> generator.jsonObject.toString():" + generator.jsonObject.toString())

            world.wireMockServer.stubFor(get(urlEqualTo("/0/public/OHLC?pair=$ticker&interval=${world.frequency*60}&since=1"))
                    .willReturn(
                            aResponse()
                                    ?.withHeader("Content-Type", "application/json")
                                    ?.withBody(generator.jsonObject.toString())
                    )
            )
        }

        Given("^the \"(\\w+_\\w+)\" stock quotation available on the platform " +
                "has a max length of \"(\\d+)\" values$") { pair: String, length: Int ->

            world.pair = pair
            val ticker = "X" + CurrencyPair(pair.replace("_", "/")).base.iso4217Currency + "Z" + CurrencyPair(pair.replace("_", "/")).counter.iso4217Currency

            var formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")
            val firstDateObject = LocalDateTime.parse(world.firstDate, formatter)
            val generator = KrakenApiResultsGenerator(world.pair, firstDateObject, world.frequency)
            generator.addQuotationsFromValuesList(generateSequence(0) { it }.take(length).toList())

            world.wireMockServer.stubFor(get(urlEqualTo("/0/public/OHLC?pair=$ticker&interval=${world.frequency*60}&since=1"))
                    .willReturn(
                            aResponse()
                                    ?.withHeader("Content-Type", "application/json")
                                    ?.withBody(generator.jsonObject.toString())
                    )
            )
        }

        Given("the server is available") {
            world.stubResponseFor4HoursFreqency = world.stubResponseFor4HoursFreqency?.withStatus(200)
        }

        Given("the error field in the server response contains one element for \"(\\w+_\\w+)\"") { pair: String ->
            world.pair = pair
            world.apiErrorGenerator = KrakenApiResultsErrorGenerator()
        }

        Given("it is not a warning") {
            val ticker = "X" + CurrencyPair(world.pair.replace("_", "/")).base.iso4217Currency + "Z" + CurrencyPair(world.pair.replace("_", "/")).counter.iso4217Currency

            world.apiErrorGenerator.setFirstError("EQuery:Unknown asset pair")
            world.wireMockServer.stubFor(get(urlEqualTo("/0/public/OHLC?pair=$ticker&interval=240&since=1"))
                    .willReturn(
                            aResponse()
                                    ?.withHeader("Content-Type", "application/json")
                                    ?.withBody(world.apiErrorGenerator.jsonObject.toString())
                    )
            )
        }

        Given("it is a warning") {
            val ticker = "X" + CurrencyPair(world.pair.replace("_", "/")).base.iso4217Currency + "Z" + CurrencyPair(world.pair.replace("_", "/")).counter.iso4217Currency

            world.apiErrorGenerator.setFirstError("WName:Some warning")
            world.wireMockServer.stubFor(get(urlEqualTo("/0/public/OHLC?pair=$ticker&interval=240&since=1"))
                    .willReturn(
                            aResponse()
                                    ?.withHeader("Content-Type", "application/json")
                                    ?.withBody(world.apiErrorGenerator.jsonObject.toString())
                    )
            )
        }

        Given("^the frequency is \"(-?\\d?)\" hours?(?: for the following table)?$") { frequency: Int ->
            world.frequency = frequency
            world.frequencyForFollowingTable = frequency
        }

        Given("^the period is \"(\\d+)\" days?$") { period: Int ->
            world.period = period
        }

        Given("^the first date is \"([\\d/ :]+)\"$") { firstDate: String ->
            world.firstDate = firstDate
        }

        Given("^the last date is \"([\\d/ :]+)\"$") { lastDate: String ->
            world.lastDate = lastDate
        }

        Given("^the current date is \"([\\d/ :]+)\"$") { currentDate: String ->
            world.currentDate = currentDate
        }

        Given("the server is not returning a response while requesting \"(\\w+_\\w+)\"") { pair: String ->
            world.pair = pair
            val ticker = "X" + CurrencyPair(pair.replace("_", "/")).base.iso4217Currency + "Z" + CurrencyPair(pair.replace("_", "/")).counter.iso4217Currency

            world.wireMockServer.stubFor(get(urlEqualTo("/0/public/OHLC?pair=$ticker&interval=240&since=1"))
                    .willReturn(
                            aResponse()
                                    ?.withFixedDelay(60000)
                    )
            )
        }

        Given("^the \"(\\w+_\\w+)\" currency is marked as \"([\\w_]+)\"") { pair: String, currentStatus: String ->
            InvestmentUniverseStatusUpdater(pair, currentStatus)
        }

        Given("^the line with the most recent date is appearing two times$") {
            // Write code here that turns the phrase above into concrete actions
            throw PendingException()
        }

        Given("the crash threshold is \"(-?\\d+.?\\d*)\"%") { crashThreshold: BigDecimal ->
            world.crashThreshold = crashThreshold
        }

        Given("that the signal for \"(\\w+_\\w+)\" on \"([\\d/ :]+)\" is \"([\\w]+)\"") {
                pair: String,
                addDateAsString: String,
                lastSignal: String ->

            var formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")
            val addDateObject = LocalDateTime.parse(addDateAsString, formatter)

            StrategiesResultsRepository().persist(listOf(StrategyResult(
                    strategy_name = "SOME_STRAT",
                    currency_pair = pair,
                    signal = lastSignal,
                    add_date = addDateObject.toEpochSecond(ZoneOffset.UTC)
            )))
        }

        Given("we bought \"(\\w+_\\w+)\" on \"([\\d/ :]+)\" at value \"(\\d+)\"") {
                pair: String,
                addDateAsString: String,
                value: Int ->

            var formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")
            val addDateObject = LocalDateTime.parse(addDateAsString, formatter)

            TransactionsUpdater(pair, BigDecimal(value), addDateObject)
        }

    }
}