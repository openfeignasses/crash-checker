import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import cucumber.api.Scenario
import cucumber.api.java8.En
import kraken.KrakenConnector
import kraken.KrakenOHLCsPersistable
import model.InvestmentUniverseStatusRetriever
import model.InvestmentUniverseStatusUpdater
import net.bytebuddy.dynamic.loading.ClassReloadingStrategy
import org.apache.commons.io.FileUtils
import repositories.StrategiesResultsRepository
import java.io.File


class StepDefsHooks(var world: World, val krakenConnector: KrakenConnector): En {

    init {
        Before(arrayOf("@CrashChecker")) { scenario: Scenario ->
            // Reset statuses
            InvestmentUniverseStatusRetriever().findAll().forEach {
                setStatusToForceIgnore(it.name)
            }
            // Reset strategies results
            StrategiesResultsRepository().truncate()
        }

        Before(arrayOf("@Stub")) { scenario: Scenario ->
            world.wireMockServer = WireMockServer()
            world.wireMockServer.start()
            world.stubResponseFor4HoursFreqency = aResponse()
            world.stubResponseFor1HourFreqency = aResponse()
            world.stubResponses = mapOf()

            krakenConnector.setUp()
            KrakenOHLCsPersistable.truncate()
        }

        After(arrayOf("@Stub")) { scenario: Scenario ->
            world.wireMockServer.stop()
        }
    }

    private fun setStatusToForceIgnore(pairName: String) {
        InvestmentUniverseStatusUpdater(pairName, "FORCE_IGNORE")
    }
}