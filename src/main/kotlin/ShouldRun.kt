import org.joda.time.DateTime
import utils.PersistenceConfigOverride
import kotlin.system.exitProcess

object ShouldRun {
    @JvmStatic
    fun main(args: Array<String>) {
        PersistenceConfigOverride.setDatabaseConnetion(
                System.getenv("DATABASE_HOST"),
                System.getenv("DATABASE_PORT"),
                System.getenv("MYSQL_DATABASE"),
                System.getenv("MYSQL_USER"),
                System.getenv("MYSQL_PASSWORD"),
                System.getenv("MYSQL_USE_SSL") ?: "true"
        )

        println(DateTime.now().toString() + " | Should the program run ? Answer: yes")
        exitProcess(0)
    }
}