package utils

import com.github.jasync.sql.db.util.size
import java.util.*
import kotlin.collections.ArrayList

class GherkinQuotationConverter(private val table: String) {

    private var lastColumnIndex: Int = 0

    fun splitString(): List<String> {
        val splittedString = table.split("\n")
        lastColumnIndex = splittedString[0].size -1
        return splittedString
    }

    private fun deleteLinesOutsideFrame(list: List<String>): List<String> {
        return list.filter { it[0] == '+' || it[0] == '|' }
    }

    fun ignoreFirstAndLastLines(list: List<String>): List<String> {
        return list.drop(1).dropLast(1)
    }

    fun keepOnlyBetweenFirstAndLastColumn(list: List<String>): List<String> {
        return list.map {
            it.substring(1, lastColumnIndex)
        }
    }

    fun convertLinesToColumns(list: List<String>): ArrayList<String> {
        var transformedResult:ArrayList<String> = arrayListOf()
        for(column_index in 0..list[0].length-1) {
            var line = ""
            for(line_index in 0..list.size-1) {
                line += list[line_index][column_index]
            }
            transformedResult.add(line.reversed())
        }
        return transformedResult
    }

    fun translateXToValues(arrayList: ArrayList<String>): List<Int> {
        var arrayList = arrayList.map { currentColumn ->
            currentColumn.indexOf("X").takeIf { it > -1 } ?: currentColumn.indexOf("x")
        }
        return arrayList
    }

    fun translateXToValuesWithDuplicates(arrayList: ArrayList<String>): List<List<Int>> {
        var arrayList = arrayList.map {
            allIndexesOf(it)
        }
        return arrayList
    }

    fun allIndexesOf(sequence: String): ArrayList<Int> {
        var indexesList: ArrayList<Int> = arrayListOf()
        for (index in 0..sequence.size-1) {
            if (sequence[index] == 'X') {
                indexesList.add(index)
            }
        }
        if (indexesList.isEmpty()) {
            indexesList.add(-1)
        }
        return indexesList
    }

    fun getValues(): List<Int> {
        var list = splitString()
        list = deleteLinesOutsideFrame(list)
        list = ignoreFirstAndLastLines(list)
        list = keepOnlyBetweenFirstAndLastColumn(list)
        var arrayList = convertLinesToColumns(list)
        return translateXToValues(arrayList)
    }

    fun getValuesWithDuplicates(): List<List<Int>> {
        var list = splitString()
        list = deleteLinesOutsideFrame(list)
        list = ignoreFirstAndLastLines(list)
        list = keepOnlyBetweenFirstAndLastColumn(list)
        var arrayList = convertLinesToColumns(list)
        return translateXToValuesWithDuplicates(arrayList)
    }
}