package kraken

import model.InvestmentUniverseStatusUpdater
import org.knowm.xchange.ExchangeFactory
import org.knowm.xchange.ExchangeSpecification
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.kraken.KrakenExchange
import org.knowm.xchange.kraken.dto.marketdata.KrakenOHLCs
import org.knowm.xchange.kraken.service.KrakenMarketDataServiceRaw

class KrakenConnector() {

    private lateinit var krakenExchange: KrakenExchange
    var httpConnTimeout: Int = 10000
    var httpReadTimeout: Int = 10000

    fun setUp() {
        val specification = ExchangeSpecification(KrakenExchange::class.java.name)
        if (System.getenv("MOCK_KRAKEN_API") == "true") {
            specification.host = "localhost"
            specification.port = 8080
            specification.sslUri = "http://localhost:8080"
        }
        specification.httpConnTimeout = httpConnTimeout
        specification.httpReadTimeout = httpReadTimeout

        krakenExchange = ExchangeFactory.INSTANCE.createExchange(specification) as KrakenExchange
    }

    fun getStockQuotations(currency_pair: CurrencyPair?, interval: Int, since: Long): KrakenOHLCs {
        require(currency_pair != null) { "currency_pair parameter cannot be null." }

        val marketDataServiceRaw = krakenExchange.marketDataService as KrakenMarketDataServiceRaw
        return marketDataServiceRaw.getKrakenOHLC(currency_pair, interval, since)
    }

    private fun updateInvestmentUniverseStatus(pairName: String, newStatus: String) {
        InvestmentUniverseStatusUpdater(pairName, newStatus)
    }
}
