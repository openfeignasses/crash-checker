package kraken

import utils.PersistenceConfigOverride
import org.knowm.xchange.kraken.dto.marketdata.KrakenOHLCs
import java.util.*
import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence
import javax.persistence.Query
import kotlin.collections.ArrayList

class KrakenOHLCsPersistable(currency_pair: String, krakenOHLCs: KrakenOHLCs) {

    var ohlcsPersistable: ArrayList<KrakenOHLCPersistable> = arrayListOf()

    init {
        krakenOHLCs.ohlCs.forEach {
            ohlcsPersistable.add(KrakenOHLCPersistable(currency_pair, it))
        }
    }

    fun persist() {
        val emFactory: EntityManagerFactory = Persistence.createEntityManagerFactory("PersistenceProviderMysql", PersistenceConfigOverride.configMap)
        val entityManager = emFactory.createEntityManager()
        entityManager.transaction.begin()
        ohlcsPersistable.forEach {
            entityManager.persist(it)
        }
        entityManager.transaction.commit()
        entityManager.close()
        emFactory.close()
    }

    companion object {
        fun findAll(pair: String): List<KrakenOHLCPersistable> {
            val emFactory: EntityManagerFactory = Persistence.createEntityManagerFactory("PersistenceProviderMysql", PersistenceConfigOverride.configMap)
            val entityManager = emFactory.createEntityManager()

            val qlQuery = "SELECT k FROM kraken.KrakenOHLCPersistable k WHERE k.currency_pair=:currency_pair"
            val query: Query = entityManager.createQuery(qlQuery).setParameter("currency_pair", pair)
            val krakenOHLCsPersistables: List<KrakenOHLCPersistable> = query.resultList as List<KrakenOHLCPersistable>

            entityManager.close()
            emFactory.close()
            return krakenOHLCsPersistables
        }

        fun findMaxAddDate(): Any? {
            val emFactory: EntityManagerFactory = Persistence.createEntityManagerFactory("PersistenceProviderMysql", PersistenceConfigOverride.configMap)
            val entityManager = emFactory.createEntityManager()

            val qlQuery = "SELECT MAX(add_date) FROM stock_quotations"
            val query: Query = entityManager.createNativeQuery(qlQuery)
            val maxAddDate = query.singleResult
            entityManager.close()
            emFactory.close()
            return maxAddDate
        }

        fun truncate() {
            val emFactory: EntityManagerFactory = Persistence.createEntityManagerFactory("PersistenceProviderMysql", PersistenceConfigOverride.configMap)
            val entityManager = emFactory.createEntityManager()
            entityManager.transaction.begin()

            val qlQuery = "TRUNCATE TABLE `cryptobot`.`stock_quotations`"
            val query: Query = entityManager.createNativeQuery(qlQuery)
            query.executeUpdate()

            entityManager.transaction.commit()
            entityManager.close()
            emFactory.close()

        }
    }
}
