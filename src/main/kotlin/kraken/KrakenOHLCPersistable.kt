package kraken

import utils.NoArg
import org.knowm.xchange.kraken.dto.marketdata.KrakenOHLC
import java.math.BigDecimal
import java.util.*
import javax.persistence.*
import kotlin.jvm.Transient

/**
 * Entity that enables persisting market data.
 */
@Entity @NoArg
@Table(name="stock_quotations")
class KrakenOHLCPersistable(currency_pair: String, krakenOHLC: KrakenOHLC) {
    @Id
    @GeneratedValue()
    private val id: Int = 0
    private val currency_pair: String = currency_pair
    val add_date: Long = Date().toInstant().epochSecond
    val time: Long = krakenOHLC.time
    val open: BigDecimal? = krakenOHLC.open
    val high: BigDecimal? = krakenOHLC.high
    val low: BigDecimal? = krakenOHLC.low
    val close: BigDecimal? = krakenOHLC.close
    val vwap: BigDecimal? = krakenOHLC.vwap
    val volume: BigDecimal? = krakenOHLC.volume
    val count: Long = krakenOHLC.count
}
