import kraken.KrakenConnector
import utils.PersistenceConfigOverride
import java.io.FileOutputStream
import java.math.BigDecimal
import java.time.LocalDateTime

fun main() {
    PersistenceConfigOverride.setDatabaseConnetion(
            System.getenv("DATABASE_HOST"),
            System.getenv("DATABASE_PORT"),
            System.getenv("MYSQL_DATABASE"),
            System.getenv("MYSQL_USER"),
            System.getenv("MYSQL_PASSWORD"),
            System.getenv("MYSQL_USE_SSL") ?: "true"
    )

    val krakenConnector = KrakenConnector()
    krakenConnector.setUp()

    val crashChecker = CrashChecker(crashThreshold = BigDecimal(System.getenv("CRASH_THRESHOLD")))
    val variations = crashChecker.runGetVariations()
    val crashingCurrencies = crashChecker.detectAndAnnounceCrash(variations)

    FileOutputStream("crash-checker.log", true).bufferedWriter().use { out ->
        out.append("============ RESULTAT DU CHECK " + LocalDateTime.now() + " =============\n")
        out.append("variations:"+variations+"\n")
        out.append("crashingCurrencies:"+crashingCurrencies+"\n")
    }
}