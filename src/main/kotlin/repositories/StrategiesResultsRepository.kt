package repositories

import model.StrategyResult
import utils.PersistenceConfigOverride
import javax.persistence.*

class StrategiesResultsRepository {
    private val entityManager: EntityManager
    private var emFactory: EntityManagerFactory = Persistence.createEntityManagerFactory(
            "PersistenceProviderMysql", PersistenceConfigOverride.configMap)

    init {
        entityManager = emFactory.createEntityManager()
    }

    fun findLastStrategiesResults(): List<StrategyResult> {
        val query: TypedQuery<StrategyResult> = entityManager
                .createQuery("SELECT s FROM model.StrategyResult s WHERE s.execution_id = " +
                        "(SELECT MAX(s.execution_id) FROM model.StrategyResult s)",
                        StrategyResult::class.java)

        return query.resultList
    }

    fun persist(strategiesResults: List<StrategyResult>) {
        entityManager.transaction?.begin()

        for (strategyResult in strategiesResults) {
            entityManager.persist(strategyResult)
        }

        entityManager.transaction?.commit()
    }

    fun truncate() {
        val emFactory: EntityManagerFactory = Persistence.createEntityManagerFactory("PersistenceProviderMysql", PersistenceConfigOverride.configMap)
        val entityManager = emFactory.createEntityManager()
        entityManager.transaction.begin()

        val qlQuery = "TRUNCATE TABLE `cryptobot`.`strategies_results`"
        val query: Query = entityManager.createNativeQuery(qlQuery)
        query.executeUpdate()

        entityManager.transaction.commit()
        entityManager.close()
        emFactory.close()
    }
}
