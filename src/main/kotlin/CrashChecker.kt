import calculator.VariationCalculator
import kraken.KrakenConnector
import model.*
import org.joda.time.DateTime
import org.knowm.xchange.currency.CurrencyPair
import repositories.StrategiesResultsRepository
import java.math.BigDecimal

class CrashChecker(crashThreshold: BigDecimal) {

    private var CRASH_THRESHOLD: BigDecimal = crashThreshold

    fun runGetVariations(): Map<String, BigDecimal> {
        val lastStrategiesResults = StrategiesResultsRepository().findLastStrategiesResults()
        val lastLongResults = lastStrategiesResults.filter { it.signal == "LONG" }
        val variations = getVariations(lastLongResults)
        return variations
    }

    private fun getVariations(lastLongResults: List<StrategyResult>): MutableMap<String, BigDecimal> {

        var variations = mutableMapOf<String, BigDecimal>()
        val tradableCurrencies = InvestmentUniverseStatusRetriever().findAll().map { it.name }
        println("tradableCurrencies:"+tradableCurrencies)
        lastLongResults.forEach { longResult ->
            val currentCurrencyPair = longResult.currency_pair
            if (tradableCurrencies.contains(currentCurrencyPair) && noPendingTransaction(longResult)) {
                variations[currentCurrencyPair] = getVariationForCurrency(currentCurrencyPair)
            }
        }

        return variations
    }

    private fun noPendingTransaction(longResult: StrategyResult): Boolean {
        val lastTransaction = TransactionsRetriever().findByPair(longResult.currency_pair)
        val result = longResult.add_date < lastTransaction.add_date
        if(!result) {
            println("Pending transaction for ${longResult.currency_pair}. Ignoring.")
        }
        return result
    }

    fun getVariationForCurrency(currencyPair: String): BigDecimal {
        val lastBuyTransaction = TransactionsRetriever().findByPair(currencyPair)
        println("========== lastBuyTransactions: " + lastBuyTransaction)

        val currencyPair = CurrencyPair(currencyPair.replace("_", "/"))
        val FREQUENCY_IN_MINS = 60
        var since: Long = DateTime().millis / 1000

        val krakenConnector = KrakenConnector()
        krakenConnector.setUp()
        val latestOhlcs = krakenConnector.getStockQuotations(currencyPair, FREQUENCY_IN_MINS, since)
        println("latestOhlcs:" + latestOhlcs)

        return VariationCalculator().getVariation(lastBuyTransaction, latestOhlcs)
    }

    fun detectAndAnnounceCrash(variations: Map<String, BigDecimal>): ArrayList<String> {
        var crashingCurrencies: ArrayList<String> = arrayListOf()
        variations.filter { isDownwardTrend(it) }.forEach {
            if(overflowsCrashThreshold(it)) {
                InvestmentUniverseStatusUpdater(it.key, "CRASHING")
                crashingCurrencies.add(it.key)
            }
        }
        return crashingCurrencies
    }

    private fun overflowsCrashThreshold(it: Map.Entry<String, BigDecimal>) =
            it.value.abs() >= CRASH_THRESHOLD.abs()

    private fun isDownwardTrend(it: Map.Entry<String, BigDecimal>) =
            it.value < BigDecimal.ZERO
}