package calculator

import model.Transaction
import org.knowm.xchange.kraken.dto.marketdata.KrakenOHLCs
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode

class VariationCalculator {
    fun getVariation(lastBuyTransaction: Transaction, latestOhlcs: KrakenOHLCs): BigDecimal {
        val currentPrice = latestOhlcs.ohlCs.last().close
        val buyPrice = lastBuyTransaction.price
        println(">>>>>>>> buyPrice: "+buyPrice)
        println(">>>>>>>> currentPrice: "+currentPrice)
        println("----- part 1:"+((currentPrice - buyPrice)))
        println("result: "+ ((currentPrice - buyPrice).divide(buyPrice, RoundingMode.DOWN)) * BigDecimal(100))
        return ((currentPrice - buyPrice).divide(buyPrice, MathContext.DECIMAL32)) * BigDecimal(100)
    }
}
