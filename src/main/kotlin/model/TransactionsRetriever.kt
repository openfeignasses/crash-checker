package model

import utils.PersistenceConfigOverride
import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence
import javax.persistence.Query

class TransactionsRetriever() {

    private lateinit var transactionsList: List<Transaction>
    lateinit var transaction: Transaction
    var entityManager: EntityManager? = null

    fun findByPair(pair: String): Transaction {
        val emFactory: EntityManagerFactory = Persistence.createEntityManagerFactory("PersistenceProviderMysql", PersistenceConfigOverride.configMap)
        entityManager = emFactory.createEntityManager()

        val qlQuery = "SELECT t FROM model.Transaction t where t.currency_pair = :currency_pair " +
                "ORDER BY t.id DESC"
        val query: Query? = entityManager?.createQuery(qlQuery)?.setParameter("currency_pair", pair)
        transaction = query?.setMaxResults(1)!!.singleResult as Transaction

        entityManager?.close()
        emFactory.close()

        return transaction
    }

    fun findAll(): List<Transaction> {
        val emFactory: EntityManagerFactory = Persistence.createEntityManagerFactory("PersistenceProviderMysql", PersistenceConfigOverride.configMap)
        entityManager = emFactory.createEntityManager()

        val qlQuery = "SELECT t FROM model.Transaction t"
        val query: Query? = entityManager?.createQuery(qlQuery)
        transactionsList = query?.resultList as List<Transaction>

        entityManager?.close()
        emFactory.close()

        return transactionsList
    }
}