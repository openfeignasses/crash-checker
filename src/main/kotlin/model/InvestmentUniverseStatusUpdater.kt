package model

import utils.PersistenceConfigOverride
import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence
import javax.persistence.Query

class InvestmentUniverseStatusUpdater(pair: String, newStatus: String) {

    val pair: String = pair
    lateinit var investmentUniverseStatus: InvestmentUniverseStatus
    var entityManager: EntityManager? = null
    init {
        val emFactory: EntityManagerFactory = Persistence.createEntityManagerFactory("PersistenceProviderMysql", PersistenceConfigOverride.configMap)
        entityManager = emFactory.createEntityManager()

        findByPair()
        investmentUniverseStatus.status = newStatus
        persist()

        entityManager?.close()
        emFactory.close()
    }

    private fun findByPair() {
        val qlQuery = "SELECT i FROM model.InvestmentUniverseStatus i where i.name = :name"
        val query: Query? = entityManager?.createQuery(qlQuery)?.setParameter("name", pair)
        investmentUniverseStatus = query?.singleResult as InvestmentUniverseStatus
    }

    private fun persist() {
        entityManager?.transaction?.begin()
        entityManager?.persist(investmentUniverseStatus)
        entityManager?.transaction?.commit()
    }
}