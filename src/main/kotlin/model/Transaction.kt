package model

import utils.NoArg
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table
import java.math.BigDecimal
import java.util.*

@Entity @NoArg
@Table(name="transactions")
data class Transaction (
        @Id
    @GeneratedValue
    val id: Int = 0,
        val currency_pair: String = "",
        val order_type: String = "",
        var price: BigDecimal = BigDecimal(-1),
        var add_date: Long = Date().toInstant().epochSecond
)
