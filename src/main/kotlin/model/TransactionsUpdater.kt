package model

import utils.PersistenceConfigOverride
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.ZoneOffset
import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence
import javax.persistence.Query

class TransactionsUpdater(currencyPair: String, newPrice: BigDecimal, addDateObject: LocalDateTime) {

    val currencyPair: String = currencyPair
    lateinit var transaction: Transaction
    var entityManager: EntityManager? = null
    init {
        val emFactory: EntityManagerFactory = Persistence.createEntityManagerFactory("PersistenceProviderMysql", PersistenceConfigOverride.configMap)
        entityManager = emFactory.createEntityManager()

        findByPair()
        transaction.price = newPrice
        transaction.add_date = addDateObject.toEpochSecond(ZoneOffset.UTC)
        persist()

        entityManager?.close()
        emFactory.close()
    }

    private fun findByPair() {
        val qlQuery = "SELECT t FROM model.Transaction t where t.currency_pair = :currency_pair"
        val query: Query? = entityManager?.createQuery(qlQuery)?.setParameter("currency_pair", currencyPair)
        transaction = query?.singleResult as Transaction
    }

    private fun persist() {
        entityManager?.transaction?.begin()
        entityManager?.persist(transaction)
        entityManager?.transaction?.commit()
    }
}