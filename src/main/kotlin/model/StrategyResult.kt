package model

import utils.NoArg
import javax.persistence.*

@Entity @NoArg
@Table(name="strategies_results")
data class StrategyResult (
    @Id
    @GeneratedValue
    val id: Int = 0,
    val strategy_name: String,
    val currency_pair: String,
    @Column(name = "\"signal\"") // escape reserved keyword
    val signal: String,
    val add_date: Long = java.util.Date().toInstant().epochSecond,
    val execution_id: Int = 1
)
