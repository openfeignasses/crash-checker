FROM maven:3.6.3-jdk-8 AS testable
RUN mkdir /app
WORKDIR /app
ENV PATH="/root/.local/bin:${PATH}"
COPY pom.xml .
RUN mvn dependency:go-offline
ADD . .
RUN mvn -o test-compile

FROM testable AS deployable
RUN mvn -o package -DskipTests=true
RUN chmod +x entrypoint.sh
ENTRYPOINT [ "./entrypoint.sh" ]
CMD java -jar /app/target/crash-checker-1.0-SNAPSHOT-jar-with-dependencies.jar