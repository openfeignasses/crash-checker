# crash-checker

This Kotlin module is in charge of detecting a crash for each currency. If there's a crash, it sends a signal to tell that we should sell the currency as soon as possible.

## Requirements

- git
- docker

## Local development

*Commands must be run from the root of the module folder.*  
*The database must be up and running (see database-migrator module).*  
*The database-migrator folder must be present next to the current module's folder, like this:*  
```
.
├── database-migrator
├── crash-checker
├── ...
```

### Execute the module

`./localci/dryrun.sh`

### Run tests

`./localci/test-unit-behaviour.sh`

Offline:  
It is possible to run tests offline, but you need to run the command while being online at least once.

```
Delivered with 💓 by


              /')                                                                           
            /' /' ____     O  ____     ,____     ____     ____     ____     ____     ____   
         -/'--' /'    )  /' /'    )   /'    )  /'    )  /'    )--/'    )--/'    )  /'    )--
        /'    /(___,/' /' /'    /'  /'    /' /'    /'  '---,    '---,   /(___,/'  '---,     
      /(_____(________(__(___,/(__/'    /(__(___,/(__(___,/   (___,/   (________(___,/      
    /'                      /'                                                              
  /'                /     /'                                                                
/'                 (___,/'                                                                  
```