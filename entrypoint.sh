#!/bin/bash

set -e

# Start in keep running mode for production
if [ -n "$KEEP_RUNNING" ]; then
    echo "Starting with keep running mode ..."
    while [ true ]
    do
        java -cp /app/target/crash-checker-1.0-SNAPSHOT-jar-with-dependencies.jar ShouldRun && \
        java -jar /app/target/crash-checker-1.0-SNAPSHOT-jar-with-dependencies.jar
        sleep 300 # loop every 5 minutes

        nb_lines=$(wc -l crash-checker.log | awk '{print $1}')
        test $nb_lines -gt 150 && \
        curl --header "Content-Type: multipart/form-data" \
             --request POST -F file=@crash-checker.log \
             https://discord.com/api/webhooks/$WEBHOOK_ID/$WEBHOOK_TOKEN && \
        echo "" > crash-checker.log # reset file
    done
fi

# By default start with what is define in CMD
exec "$@"